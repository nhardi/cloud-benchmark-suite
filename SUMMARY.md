# Contents

- [Benchmarking](README.md)
- [How it works](docs/HowItWorks.md)
- [Requirements](docs/Requirements.md)
- [How to run](docs/HowToRun.md)
- [Results](docs/Results.md)
    - [Kibana 4 - 101](docs/Kibana4-101/Kibana4-101.md)
        - [See My Data - Tutorial](docs/Kibana4-101/SeeMyData.md)
        - [Create Visualizations - Tutorial](docs/Kibana4-101/CreateVisualizations.md)
        - [Dashboards - Tutorial](docs/Kibana4-101/Dashboards.md)
