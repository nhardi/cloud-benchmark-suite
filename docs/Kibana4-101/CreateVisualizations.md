# Create Visualizations - Tutorial

Dashboards are made of pre-made visualizations and searches. These need to be
created and saved one by one.

Click on the **Visualize** tab and select the kind of chart/widget/data to create:

![Kibana Visualize](./Visualize.png)

**NOTE**: one can also modify existing visualizations. If that's the case, **please**
make sure **not to overwrite**.


## Example 1 - Data Table aggregated by cloud

Let's build a data table to check the aggregated benchmark results per cloud.
Select **Data Table** from the list of visualizations and let's start from a
fresh new **search** (no filters):

**NOTE**: _sdccloud-int_ has been renamed to _bmkwg-int_
![Kibana Visualize from Search](./VisFromSearch.png)

We want our table to be grouped by cloud and showing the average scores from all
the benchmarks done there. So in theory it should look something like this:

| CLOUD_NAME | NUM_BMKS | AVG(DB12) | AVG(WHETSTONE) | AVG(KV) | Min(KV) | Max(KV) | ... |
| --- | --- | --- | --- | --- | --- |
| MyCloud1 |  | | | | | | |
| YourCloud2 | | | | | | | | |

So these are the steps in Kibana:

 1. Aggregate by count to get _NUM BMKS_
 2. Sub-aggregate by splitting the rows by the term _message.metadata.cloud_
 3. Now get the benchmark scores per cloud, by adding aggregations for each
 desired score:
  * Aggregate by the average of the term _message.profiles.whetsone.score_
  * Aggregate by the average of the term _message.profiles.DB12.value_
  * Aggregate by the average of the term _message.metadata.bogomips_
  * Aggregate by the avg, min and max of the term _message.profiles.rkv.evt.cpu_values_

In the UI it will look something like the following:

![Kibana Visualize Data Table](./DataTable.png)


## Example 2 - Line chart with the (_5%, avg, 95%_) benchmark scores for cloud "_test_", per node

We want a plot that shows 3 different lines, containing one data point per node
in cloud "_test_". These data points will refer to the 5%, average and 95% scores
of the benchamrks performed on those nodes.

Unlike the previous example, here we are looking at creating a visualization
from a filter (where the filter refers to the cloud name being _test_). This means
one cannot simply start its visualization for a new search!

#### Create new search

Let's create a **Search** first. So we want to:
 * Search by "test"
 * Select the field _message.metadata.cloud_
 * Focus only on the results with the parameter value _test_
 * Save search


![Kibana New Search](./NewSearch.png)


#### Create visualization from that search

After selecting **Line Chart**, look at the existing searcher and select the
one from above:

![Kibana New Visualization from Saved Search](./VisFromSavedSearch.png)

_For simplicity, let's just consider the numeric aggregation for one type of
benchmark. The process is reproducible for any other numeric field._

At this point one wants 2 different aggregations: the _average_ and the _percentile_.
Choose **5** and **95** from the percentile options and then add a
sub-aggregation to split the X axis by node (_UID_):

![Kibana Line Chart](./Percentiles.png)

--*
