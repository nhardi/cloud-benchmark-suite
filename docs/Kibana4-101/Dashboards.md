# Dashboards - Tutorial

Users can either profit from existing dashboards or simply create their own.

These features are available under the **Dashboard** tab.

## Viewing existing dashboards

Load an existing dashboard by clicking on the **folder icon** and select one from
the list _(note the naming convention with the index name as a prefix to better
better identify the dashboards)_:

![Kibana Load Dashboard](./LoadDashboard.png)


## Creating dashboards

Click on the **new icon** to start creating a new dashboard:

![Kibana New Dashboard](./NewDashboard.png)

By clicking now on the **plus icon** one is able to add existing visualizations
and searches:

![Kibana Add Visualization](./AddVis.png)


One can _move_, _scale_ and _edit_ these visualization directly from the dashboard
creation.

To save the dashboard simply click on the **save icon** and remember to name it
consistently with the existing dashboards and your own use case tag.

![Kibana Save Dashboard](./SaveDashboard.png)
