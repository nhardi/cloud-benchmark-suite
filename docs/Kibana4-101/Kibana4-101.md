# Kibana 4 - 101

Once users have access to [Kibana](https://es-bmkwg.cern.ch), they are
free to look at any data and create their own dashboards and visualizations.

Some of the key and basic features of Kibana are explained in the following
sub-chapters.

At this point it is assumed users have full access to Kibana 4:

![Kibana UI](./Kibana4.png)
