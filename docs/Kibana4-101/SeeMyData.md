# See My Data - Tutorial

Currently, there are no index level ACLs in Kibana, which means all users will
be able to see data from other indexes in the same ES instance.

For the Benchmark Suite, the index users must select is **bmkwg-int**.

So, to read the raw messages from the benchmarks, one need to click on the
**Discover** tab and select the proper index name:

![Kibana Discover](./Discover.png)

This will show all of the finished benchmarks, everywhere, on the past time
window defined on the top right corner.

To quickly filter some specific messages, one needs to use the **search bar**:

![Kibana Search](./Search.png)


By default the presented results will always be a table with the full messages.
To organize it and show only your preferred parameters, please use the **fields**
section on the left:

![Kibana Fields](./Fields.png)
