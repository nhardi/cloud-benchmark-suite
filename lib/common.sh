#
#  Copyright (c) CERN 2016
#
#  Author: Cristovao Cordeiro
# 

# Help display
usage='Usage:
 $0 [OPTIONS]

OPTIONS:
\n -q
\t Quiet mode. Do not prompt user
\n -o
\t Offline mode. Do not publish results. If not used, the script expects the publishing parameters
\n -i
\t Solves/checks the general and unique dependencies for the specified --benchmarks. If not used, assumes all the dependencies are already installed and configured. NOTE: should run as root
\n --benchmarks=<bmk1;bmk2>
\t (REQUIRED) Semi-colon separated list of benchmarks to run. Available benchmarks are:
\t\t - kv
\t\t - whetstone
\t\t - DB12
\t\t - hyper-benchmark (*)
\n --mp_num=#
\t Number of concurrent processes (usually cores) to run. If not used, mp_num = cpu_num
\n --kv_xml=<xmlFile>
\t Input file for the KV benchmark. If not provided, SingleMuonGenerator is default
\n --uid=<id>
\t (Optional) Unique identifier for the host running this script. If not specified, it will be generated
\n --public_ip=<ip>
\t (Optional) Public IP address of the host running this script. If not specified, it will be generated
\n --cloud=<cloudName>
\t Cloud name to identify the results - if not specified, CLOUD=test and use -q to avoid prompt
\n --vo=<VO>
\t (Optional) Name of the VO responsible for the underlying resource
\n --pnode=<physicalNode>
\t (Optional) Name of the hypervisor machine hosting the VM
\n --queue_port=<portNumber>
\t Port number of the ActiveMQ broker where to send the benchmarking results
\n --queue_host=<hostname>
\t Hostname with the ActiveMQ broker where to send the benchmarking results
\n --username=<username>
\t Username to access the ActiveMQ broker where to send the benchmarking results
\n --password=<password>
\t User password to access ActiveMQ broker where to send the benchmarking results
\n --amq_key=<path_to_key>
\t Key file for the AMQ authentication, without passphrase. Expects --amq_cert
\n --amq_cert=<path_to_cert>
\t Certificate for the AMQ authentication. Expects --amq_key
\n --topic=<topicName>
\t Topic (or Queue) name used in the ActiveMQ broker
\n --freetext=<string>
\t (Optional) Any additional free text to add to the generated output JSON
\n\n\t (*) this benchmark performs the following measurements sequence:
 1-min Load -> read machine&job features -> DB12 -> 1-min Load -> whetstone -> 1-min Load
'

# Execution Directory
DIRNAME=`readlink -m ${BMK_LOGDIR:-"/tmp/$(basename $0)_$(whoami)"}`

LOG="$DIRNAME/profiler.sh.out"
LOCK_FILE="$DIRNAME/$(basename $0).lock"

# If the script is running exit
if [ -e $LOCK_FILE ]; then
 echo "Exiting because of $0 already running. $LOCK_FILE exists and last time modified: $(stat -c %y $LOCK_FILE)"
 exit 0
fi

[ -e $DIRNAME ] && rm -rf $DIRNAME
mkdir -p $DIRNAME
chmod 777 $DIRNAME

touch $LOCK_FILE || (echo "Can't create lock file $LOCK_FILE. Exiting..." && exit 0)

# Saves file descriptors for later being restored
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
# Redirect stdout and stderr to a log file
exec 1>$LOG 2>&1

# Set and trap a function to be called in always when the scripts exits in error
function onEXIT {
  # Delete lock file
  [ -e $LOCK_FILE ] && rm -fr $LOCK_FILE

  # Save workdir and clean
#  cd $DIRNAME && mkdir -p _previous_bmk_results_$(whoami)
#  tar -czf bmk_out_`date +"%d%m%Y_%s"`.tar.gz $DIRNAME 2>/dev/null
#    rm -fr $RUNAREA_PATH $DIRTMP $PARSER_PATH && mv bmk_out*.tar.gz $PREVIOUS_RESULTS_DIR
#  fi

  if [ $END -eq 0 ]; then
    echo -e "\n
!! ERROR !!: \nThe script encountered a problem. Exiting without finishing.
Log snippet ($LOG):
***************************\n" >&3
    tail -15 $LOG >&3
    echo -e "\n***************************" >&3
  else
    echo "INFO: Finished benchmark"
    echo -e "\nExiting...\n" >&3
  fi
}
trap onEXIT EXIT

# Get parameters
QUIET=0
CLOUD='test'
KV_XML_DEFAULT=''
FREE_TEXT=''
while [ "$1" != "" ]; do
  case $1 in
    -q    )                 QUIET=1;
    ;;
    -o    )                 OFFLINE=1;
    ;;
    -i  )                   INSTALLS=1;
    ;;
    --benchmarks=*  )       BENCHMARKS=${1#*=};
    ;;
    --mp_num=*  )           MP_NUM=${1#*=};
    ;;
    --kv_xml=*  )           KV_XML_DEFAULT=${1#*=};
    ;;
    --uid=*    )            VMUID=${1#*=};
    ;;
    --public_ip=* )         PUBLIC_IP=${1#*=};
    ;;
    --cloud=* )             CLOUD=${1#*=};
    ;;
    --vo=* )                VO=${1#*=};
    ;;
    --pnode=* )             PNODE=${1#*=};
    ;;
    --queue_port=* )        QUEUE_PORT=${1#*=};
    ;;
    --queue_host=* )        QUEUE_HOST=${1#*=};
    ;;
    --username=* )          QUEUE_USERNAME=${1#*=};
    ;;
    --password=* )          QUEUE_PASSWORD=${1#*=};
    ;;
    --amq_key=* )           AMQ_KEY=${1#*=};
    ;;
    --amq_cert=* )          AMQ_CERT=${1#*=};
    ;;
    --topic=* )             QUEUE_NAME=${1#*=};
    ;;
    --freetext=* )          FREE_TEXT=${1#*=};
    ;;
    -h )        echo -e "${usage}" >&3
    END=1
    exit 1
    ;;
    * )         echo -e "Invalid option $1 \n\n${usage}" >&3
    END=1
    exit 1
  esac
  shift
done


# No point moving forward if bmks are not specified
if [[ -z $BENCHMARKS ]]
then
  echo "No benchmarks provided. Please use --benchmarks. Exiting..." >&3
  echo "WARN: --benchmarks not specified: $BENCHMARKS. Exit"
  exit 1
else
  bmks=$(echo $BENCHMARKS | tr ";" "\n")
fi

# Exit when any command fails. To allow failing commands, add "|| true"
set -o errexit
set -x

echo "
  #######################################
  ###    CERN Benchmarking Suite      ###
  #######################################
"

echo "Log file at: $LOG" >&3

NUM_CPUS=`grep -c processor /proc/cpuinfo`

if [[ ! -z $INSTALLS ]]
then
  echo "INFO: installing dependencies..."
  if [ "$(id -u)" -ne "0" ]
  then
    echo "WARN: you're not running as root. Please make sure $(whoami) has the right permissions or the installations will fail." >&3
  fi
  source $ROOTDIR/lib/dependencies.sh
  base_dependencies
  for install_bmk in $bmks
  do
    if [[ $install_bmk =~ ^(compress-7zip|encode-mp3|x264|build-linux-kernel)$ ]]
    then
      #phoronix_dependencies
      echo "TODO"
    elif [[ $install_bmk == "kv" ]]
    then
      kv_dependencies
    elif [[ $install_bmk =~ ^(whetstone)$ ]]
    then
      unixbench_dependencies
    else
      :
    fi
  done
  echo "INFO: Finished installing dependencies"
fi


echo "`date`: Starting benchmark..."

if [[ -z $VMUID ]]
then
  if [ -f /proc/sys/kernel/random/boot_id  ]
  then
    VMUID=`hostname -s`_`cat /proc/sys/kernel/random/boot_id`
  else
    VMUID=`hostname -s`_$(date -d  "`who -b | sed -e 's@system boot@@'`" +%s)
  fi
fi

if [[ -z $PUBLIC_IP ]]
then
  if ! hash ifconfig 2>/dev/null
  then
    call_ifconfig=`whereis ifconfig | awk -F' ' '{print $2}'`
  else
    call_ifconfig="ifconfig"
  fi

  if [[ "$($call_ifconfig eth0)" == *"eth0"* ]]
  then
    PUBLIC_IP=`$call_ifconfig eth0 | grep "inet " | awk -F' ' '{print $2}' | awk -F':' '{print $NF}'`
  else
    echo 'WARN: could not find eth0 IP address. IP parameter not defined!'
  fi
fi

if [ $CLOUD == "test" ] && [ $QUIET -eq 0 ]
then
  echo "CLOUD name is set to 'test'. To change it write a new cloud name:" >&3
  read -p "" -r
  if [ ! -z $REPLY ]
  then
    CLOUD=$REPLY
  fi

  echo "CLOUD name is $CLOUD" >&3
fi

# Set auxiliary directories and variables
DIRTMP="$DIRNAME/bmk_tmp"
KV_FILE_PATH="$DIRTMP/KVbmk.xml"
TIMES_SOURCE_PATH="$DIRTMP/times.source"
PARSER_PATH="$DIRTMP/parser"
RUNAREA_PATH="$DIRNAME/bmk_run"
RESULTS_FILE="$DIRTMP/result_profile.json"
PREVIOUS_RESULTS_DIR="$DIRNAME/_previous_bmk_results"
UNIX_BENCH="$ROOTDIR/byte-unixbench/UnixBench"

[ -e $DIRTMP ] && rm -rf $DIRTMP
mkdir -p $DIRTMP
chmod 777 $DIRTMP

if [[ ! -z $MP_NUM ]] && [ $MP_NUM -ne $NUM_CPUS ]
then
  export BENCHMARK_TARGET="core"
else
  export BENCHMARK_TARGET="machine"
  MP_NUM=$NUM_CPUS
fi
echo "export BENCHMARK_TARGET=$BENCHMARK_TARGET" > $TIMES_SOURCE_PATH

function write_parser {

  #Parse the tests
  cat <<X5_EOF >$PARSER_PATH
source $TIMES_SOURCE_PATH
export DB12=$DB12
export HWINFO=$HWINFO
export FREE_TEXT=$FREE_TEXT
export PNODE=$PNODE
export MP_NUM=$MP_NUM
python $wrapper_basedir/parser.py -i $VMUID -c $CLOUD -v $VO -f $RESULTS_FILE -p $PUBLIC_IP -d $RUNAREA_PATH
X5_EOF

  chmod ugo+rx $PARSER_PATH
}

function print_results {
  # Expects all the variables below to be set

  echo -e "\n\n=========================================================" >&3
  echo -e "RESULTS OF THE OFFLINE BENCHMARK FOR CLOUD $CLOUD" >&3
  echo -e "=========================================================" >&3
  if [[ ! -z $HWINFO ]]
  then
    echo -e "\t\nMachine classification: $HWINFO " >&3
  fi
  if [[ ! -z $DB12 ]]
  then
    echo -e "\t\nDIRAC Benchmark = $DB12 (est. HS06)" >&3
  fi
  if [[ ! -z $WHETS ]]
  then
    echo -e "\t\nWhetstone Benchmark = $WHETS (MWIPS)" >&3
  fi
  if [[ ! -z $HYPER_BENCHMARK ]]
  then
    echo -e "\t\nHyper-Benchmark results (ordered by execution):" >&3
    echo -e "\tDIRAC Benchmark = $HYPER_DB12 (est. HS06)" >&3
    echo -e "\tWhetstone Benchmark = $HYPER_WHETS (MWIPS)" >&3
    echo -e "\t1-min Load measurements = $HYPER_1minLoad_1 ; $HYPER_1minLoad_2 ; $HYPER_1minLoad_3" >&3
    echo -e "\tmachinefeatures-HS06 = $HYPER_MACHINEFEATURES_HS06" >&3
    echo -e "\tjobfeatures-HS06_job = $HYPER_JOBFEATURES_HS06" >&3
    echo -e "\tjobfeatures-allocated_cpu = $HYPER_JOBFEATURES_ALLOCATED_CPU" >&3
  fi
  # If at least one CPU KV log is there...
  if [ `ls $RUNAREA_PATH/KV/kvtest_*/KV.thr.1/data/*/*log` ]
  then
    echo -e "\t\nKV Benchmark:" >&3
    grep -A1 "INFO Statistics for 'evt'" $RUNAREA_PATH/KV/kvtest_*/KV.thr.*/data/*/*log | grep "<cpu>" | awk 'BEGIN{amin=1000000;amax=0;}{count+=1; val=int($5)/1000.; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val}END{sep=sprintf("%*s", 100, "");gsub(/ /, "", sep);printf "%sKV cpu performance [sec/evt]: avg  %.2f over %d threads. Min Value %.2f Max Value %.2f\n%s", sep, sum/count, count, amin, amax, sep}' >&3
  fi
}

function get_classification {
  # replaces hwinfo.rb
  vendor_id=`lscpu | grep "Vendor ID" | awk -F' ' '{print $NF}'`
  if [[ $vendor_id == "GenuineIntel" ]]
  then
    vendor="i"
  elif [[ $vendor_id == "AuthenticAMD" ]]
  then
    vendor="a"
  else
    vendor="o"
  fi

  osmajorrelease=$(cat /etc/redhat-release | cut -d "." -f 1 | awk '{print $NF}')

  cpus=${NUM_CPUS:-$(grep -c processor /proc/cpuinfo)}
  cpufamily=`lscpu | grep "CPU family" | awk -F' ' '{print $NF}'`
  cpumodel=`lscpu | grep "Model:" | awk -F' ' '{print $NF}'`
  cpu_stepping=`lscpu | grep Stepping | awk -F' ' '{print $NF}'`
  cpu_speed=`lscpu | grep MHz | awk -F' ' '{print $NF}'`

  echo ${vendor}${osmajorrelease}_${cpus}_f${cpufamily}m${cpumodel}s${cpu_stepping}_mhz${cpu_speed}
}

function run_whets {
  # Expects all the variables below to be set
  # Also has optional $1 as basedir for the results
  whetstone_results=${1:-$RUNAREA_PATH"/whets"}
  whetstone_results_file=$whetstone_results/whets.res

  [ -e $whetstone_results ] && rm -fr $whetstone_results
  mkdir -p $whetstone_results

  for (( i=1 ; i<=$MP_NUM ; i++ ))
  do
    $ROOTDIR/run/whets >> $whetstone_results_file &
  done
  wait

  grep MWIPS $whetstone_results_file | awk -F' ' '{print $2}' | tr '\n' ' ' | awk '{sum=0;for (i=1;i<=NF;i++) sum+=$i;avg=sum/NF;print avg}'
}

function run_DB12 {
  # Expects all the variables below to be set
  # Also has optional $1 as basedir for the results
  DB12_RUNAREA=${1:-$RUNAREA_PATH"/DB12"}

  [ -e $DB12_RUNAREA ] && rm -rf $DB12_RUNAREA
  mkdir -p $DB12_RUNAREA

  cp -f "$ROOTDIR/run/DB12.py" $DB12_RUNAREA

  python $DB12_RUNAREA/DB12.py --cpu_num=$MP_NUM
}
