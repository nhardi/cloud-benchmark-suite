function dump_kv_SingleMuonSim {
  KV_FILE=$1
  # Writes the XML file for the KV in the directory specified in $1
  cat > $KV_FILE << 'EOF'
<?xml version="1.0"?>
<!DOCTYPE unifiedTestConfiguration SYSTEM "http://www.hep.ucl.ac.uk/atlas/AtlasTesting/DTD/unifiedTestConfiguration.dtd">

<unifiedTestConfiguration>

  <kv>
    <kvtest name='AtlasG4SPG' enabled='true'>
      <release>ALL</release>
      <priority>20</priority>
      <kvsuite>KV2012</kvsuite>
      <trf>AtlasG4_trf.py</trf>
      <desc>Single Muon Simulation</desc>
      <author>Alessandro De Salvo [Alessandro.DeSalvo@roma1.infn.it]</author>
      <outpath>${T_DATAPATH}/SimulHITS-${T_RELEASE}</outpath>
      <outfile>${T_PREFIX}-SimulHITS-${T_RELEASE}.pool.root</outfile>
      <logfile>${T_PREFIX}-SimulHITS-${T_RELEASE}.log</logfile>
      <kvprestage>http://kv.roma1.infn.it/KV/input_files/simul/preInclude.SingleMuonGenerator.py</kvprestage>
      <signature>
        outputHitsFile="${T_OUTFILE}" maxEvents=100 skipEvents=0 preInclude=KitValidation/kv_reflex.py,preInclude.SingleMuonGenerator.py geometryVersion=ATLAS-GEO-16-00-00 conditionsTag=OFLCOND-SDR-BS7T-04-03
      </signature>
      <copyfiles>
        ${T_OUTFILE} ${T_LOGFILE} PoolFileCatalog.xml metadata.xml jobInfo.xml
      </copyfiles>
      <checkfiles>${T_OUTPATH}/${T_OUTFILE}</checkfiles>
    </kvtest>
  </kv>
</unifiedTestConfiguration>
EOF

  echo "KV: wrote Single Muon Simulation XML in ${KV_FILE}"
}


function run_kv {
  # Receives the following arguments:
  #   - $1 is the TIMES_SOURCE file path
  #   - $2 is the KV XML file path
  #   - $3 is the Cloud name
  #   - $4 is the path where the benchmark is running
  #   - $5 is the current path, where sw-mgr might be

  TIMES_SOURCE=$1
  KV_FILE=$2
  CLOUD_NAME=$3
  RUNAREA="$4/KV"
  ROOTDIR=$5

  echo "export init_kv_test=`date +%s`" >> $TIMES_SOURCE
  KVBMK="file://$KV_FILE"
  KVTAG="KV-Bmk-$CLOUD_NAME"
  KVTHR=${MP_NUM:-$(grep -c processor /proc/cpuinfo)}

  [ -e $RUNAREA ] && rm -rf $RUNAREA
  mkdir -p $RUNAREA

  SW_MGR_aux="$ROOTDIR/sw-mgr"
  SW_MGR="$RUNAREA/sw-mgr"
  if [[ -e $SW_MGR_aux ]]
  then
    cp $SW_MGR_aux $RUNAREA
  else
    ( cp -f /cvmfs/atlas.cern.ch/repo/benchmarks/bin/sw-mgr $SW_MGR ) || ( wget https://kv.roma1.infn.it/KV/sw-mgr --no-check-certificate -O $SW_MGR )
  fi

  chmod u+x $SW_MGR

  # TODO: understand if is possible to execute sw_mgr to store results in destination folder != ./
  cd $RUNAREA

  export VO_ATLAS_SW_DIR=/cvmfs/atlas.cern.ch/repo/sw
  echo 'source /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0/cmtsite/asetup.sh --dbrelease=current AtlasProduction 17.8.0.9 opt gcc46 slc6 64'
  source /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0/cmtsite/asetup.sh --dbrelease=current AtlasProduction 17.8.0.9 opt gcc46 slc6 64 || true


  SW_MGR_START=`date +"%y-%m-%d %H:%M:%S"`
  echo "start sw-mgr ${SW_MGR_START}"

  KVSUITE=`grep -i "<kvsuite>" $KV_FILE | head -1 | sed -E "s@.*>(.*)<.*@\1@"`
  echo KVBMK $KVBMK
  echo KVSUITE $KVSUITE

  echo "./sw-mgr -a 17.8.0.9-x86_64 --test 17.8.0.9 --no-tag -p /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0 --kv-disable ALL --kv-enable $KVSUITE --kv-conf $KVBMK --kv-keep --kvpost --kvpost-tag $KVTAG --tthreads $KVTHR "

  REFDATE=`date +\%y-\%m-\%d_\%H-\%M-\%S`
  KVLOG=kv_$REFDATE.out
  ./sw-mgr -a 17.8.0.9-x86_64 --test 17.8.0.9 --no-tag -p /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0 --kv-disable ALL --kv-enable $KVSUITE --kv-conf $KVBMK --kv-keep --tthreads $KVTHR > $KVLOG

  TESTDIR=`ls -tr | grep kvtest_ | tail -1`
  df -h > space_available.log
  tar -cvjf ${TESTDIR}_${REFDATE}.tar.bz2 ${TESTDIR}/KV.thr.*/data/*/*log $KVLOG space_available.log
  SW_MGR_STOP=`date +"%y-%m-%d %H:%M:%S"`
  echo "end sw-mgr ${SW_MGR_STOP}"

  PERFMONLOG=PerfMon_summary_`date +\%y-\%m-\%d_\%H:\%M:\%S`.out
  echo "host_ip: `hostname`" >> $PERFMONLOG
  echo "start sw-mgr ${SW_MGR_START}">> $PERFMONLOG
  echo "end sw-mgr ${SW_MGR_STOP}" >> $PERFMONLOG
  grep -H PerfMon $TESTDIR/KV.thr.*/data/*/*log >> $PERFMONLOG

  echo "export end_kv_test=`date +%s`" >> $TIMES_SOURCE

  cd $ROOTDIR
}
