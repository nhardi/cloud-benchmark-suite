#
#  Copyright (c) CERN 2016
#
#  Author: Cristovao Cordeiro
# 

try:
    import stomp
except ImportError:
    import sys
    sys.path.append('/cvmfs/sft.cern.ch/lcg/releases/LCG_85swan2/stomppy/3.1.3/x86_64-centos7-gcc49-opt/lib/python2.7/site-packages/')
    import stomp
except:
    print "Couldn't import stomp"
    raise
import argparse
import time
import os

def send_message(ssl_flag, args, stomp_mversion):
    conn = stomp.Connection([(args.server, int(args.port))], use_ssl=ssl_flag, \
        ssl_key_file=args.key_file, ssl_cert_file=args.cert_file, ssl_version=3)

    conn.start()
    conn.connect(login=args.username, passcode=args.password)

    if stomp_mversion == 3:
        conn.send(file, destination=args.name)
    else:
        conn.send(body=file, destination=args.name)

    time.sleep(2)
    conn.disconnect()


if __name__ == '__main__':
    stomp_mversion = stomp.__version__[0]

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", nargs='?', help="Queue port")
    parser.add_argument("-s", "--server", nargs='?', help="Queue host")
    parser.add_argument("-u", "--username", nargs='?', help="Queue username")
    parser.add_argument("-w", "--password", nargs='?', help="Queue password")
    parser.add_argument("-n", "--name", nargs='?', help="Queue name")
    parser.add_argument("-k", "--key_file", nargs='?', help="AMQ authentication key")
    parser.add_argument("-c", "--cert_file", nargs='?', help="AMQ authentication certificate")
    parser.add_argument("-f", "--file", nargs='?', help="File to send", default="result_profile.json")
    args = parser.parse_args()

    file = open(args.file,'r').read()

    try:
        if os.path.isfile(args.key_file) and os.path.isfile(args.cert_file):
            ssl = True
            print "AMQ SSL: certificate based authentication"
            send_message(ssl, args, stomp_mversion)
        else:
            raise IOError("AMQ: The pair key-certificate files does not exist")
    except IOError as e:
        print e
        print "AMQ: Going with user-password..."
        ssl = False
        print "AMQ Plain: user-password based authentication"
        send_message(ssl, args, stomp_mversion)
    except stomp.exception.NotConnectedException as e:
        print e
        print "AMQ: SSL connection failed, trying with plain user-password\n\
                WARNING: beware that usually the port numbers change according to the authentication method"
        ssl = False
        print "AMQ Plain: user-password based authentication"
        send_message(ssl, args, stomp_mversion)
    except:
        print "AMQ: unkown error while publishing message"
        raise
